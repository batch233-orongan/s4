========================================
SQL - Advance Selects and Joining Tables
========================================

-- Mini-activity: 20 mins
     --insert the following records in their respective tables
     -- Taylor Swift
         -- Album: Fearless 2008-11-11 
             --Songs: Fearless, 402, Pop Rock 
                   -- Love Story, 355, Country Pop
         -- Album: Red, 2012-10-22 
             -- Songs: State of Grace, 455, Rock, alternative rock, arena rock
                    -- Red, 341, Country
     -- Lady Gaga
         -- Album: A Star Is Born, 2018-10-05 
             -- Songs: Black Eyes, 304, Rock and roll 
                    -- Shallow, 336, Country, rock, folk rock
         -- Album: Born This Way, 2011-05-23 
             -- Song: Born This Way, 420, Electropop
     -- Justin Bieber
         -- Album: Purpose, 2015-11-13 
             -- Song: Sorry, 320, Dancehall-poptropical housemoombahton
         -- Album: Believe, 2012-06-15 
             -- Song: Boyfriend, 252, Pop
     -- Ariana Grande
         -- Album: Dangerous Woman, 2016-05-20 
             -- Song: Into You, 405, EDM House
         -- Album: Thank U, Next, 2019-02-08  
             -- Song: Thank U, Next, 327, Pop, R&B
     -- Bruno Mars
         -- Album: 24k Magic, 2016-11-18 
             -- Song: 24k Magic, 346, Funk, disco, R&B
         -- ALbum: Earth to Mars,2011-02-07 
             -- Song: Lost, 321, Pop

SOLUTION

-- Artist --

INSERT INTO artists (artistName) VALUES ("Taylor Swift");
INSERT INTO artists (artistName) VALUES ("Lady Gaga");
INSERT INTO artists (artistName) VALUES ("Ariana Grande");
INSERT INTO artists (artistName) VALUES ("Justin Bieber");
INSERT INTO artists (artistName) VALUES ("Bruno Mars");

-- Artist per Album --

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 5);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 6);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 6);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 7);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 8);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 8);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24k Magic", "2016-11-18", 9);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-02-07", 9);


-- songs per album per artist --
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 402, "Pop Rock", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 402, "Country Pop", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 455, "alternatve rock, arena rock", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 341, "Country", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 304, "Rock and Roll", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 336, "Country, rock, folk rock", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 420, "Electropop", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical housemoombahton", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 252, "Pop", 10);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 405, "EDM House", 11);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U, Next", 327, "Pop, R&B", 12);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24k Magic", 326, "Funk, Disco , R&B", 13);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 321, "Pop", 14);


1. Exclude Records
	Terminal
	Syntax:
		SELECT column_name FROM table_name WHERE column_name != value;

	Example:
		SELECT * FROM songs WHERE id != 11;
		SELECT * FROM songs WHERE album_id != 8 AND album_id != 9;

2. Finding Records using comparison operators
	Terminal
	Syntax:
		SELECT column_name FROM table_name WHERE condition;
	Example:
		SELECT * FROM songs WHERE length > 230;
		SELECT * FROM songs WHERE length < 200;
		SELECT * FROM songs WHERE length > 230 OR length < 200;
		SELECT * FROM songs WHERE genre = "Pop";

3. Getting records with specific conditions
	Terminal
	Syntax:
	-- can be used for querying multiple columns--
		SELECT column_name FROM table_name WHERE condition;

	Example: 
		SELECT* FROM songs WHERE id = 6 OR id = 7 OR id = 8;

	Syntax:
		SELECT column_name FROM table_name WHERE column_name IN (values);

	Example:
		SELECT * FROM songs WHERE id IN (6, 7, 8);
		SELECT * FROM songs WHERE genre IN ("Pop", "Electropop", "EDM House");

4. Show records with partial match
	Terminal
		LIKE clause
		Percent (%) symbols and underscore (_) are called wildcard operators
			% -> represents zero or multiple characters.
			_ -> represents a single character.

		a. Find a value with a match at the start
		SELECT * FROM songs WHERE song_name LIKE "th%";

		b. Find values with a match at the end.
		SELECT * FROM songs WHERE song_name LIKE "%ce";

		c. Find values with a match at any position;
		SELECT * FROM songs WHERE song_name LIKE "%or%";

		d. Find values with a match of a specific length/pattern
		SELECT * FROM songs WHERE song_name LIKE "__rr_";

		e. Find a value	 with a match at certain positions.
		SELECT * FROM albums WHERE album_title LIKE "_ur%";

5. Sort Records
	Terminal
	Syntax	:
		SELECT column_name	FROM table_name ORDER BY column_name ORDER;
	Example:
		-- ASC -> ascending --
		-- DESC -> descending --

		SELECT * FROM songs ORDER BY song_name;
		SELECT * FROM songs ORDER BY song_name ASC;
		SELECT * FROM songs ORDER BY length DESC;

6. Limiting Records
	Terminal
		SELECT * FROM songs LIMIT 5;

7. Showing records with distinct values
	Terminal
		-- using DISTINCT keyword to avoid repetition of values from retrieval
		SELECT genre FROM songs;
		SELECT DISTINCT genre FROM songs;

8. Joining two tables
	Terminal
	Syntax:	
		SELECT column_name FROM	table1
		JOIN table2 ON table1.id = table2.foreign_key_column
	Example:
		SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id;

9. Joining multiple tables
	Terminal
	Syntax:
		SELECT column_name FROM table1
		JOIN table2	ON table1.id = table2.foreign_key_column
		JOIN table2	ON table1.id = table2.foreign_key_column;

	Example:
		SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id
		JOIN songs ON albums.id = songs.album_id;

10. Joining table with specifie WHERE conditions
	Example:
		SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE artistName	 LIKE "%a%";

11. Selecting columns to be displayed from joining tables
	Example
		SELECT artistName, album_title, date_released, song_name, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

12. Providing aliases for joining tables
	Terminal
	Syntax:
		SELECT column_name AS alias FORM table1
		JOIN table2 ON table1.id = table2.foreign_key_column
		JOIN table3 ON table2.id = table3.foreign_key_column;

	Example:
		SELECT artistName AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

13. Displaying Data from joining tables
	-- Create user information, a playlist and songs added to the user playlist --
	INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("manex", "man123", "Emmanuel Orongan", 1123456789, "eman@gmail.com", "Cebu City");

	INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2022-09-20 01:00:00");

	INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 8), (1, 14), (1, 10);

	Joining multiple tables

	SELECT * FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;

	Selecting specific columns to be displayed from the query:

	SELECT user_id, datetime_created, song_name, length, genre, album_id FROM playlists JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;